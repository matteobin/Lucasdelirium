// @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache 2.0
document.head.innerHTML += '<link rel="stylesheet" href="stile-js.css">';
function addClass(element, className) {
	if (element.classList) {
		element.classList.add(className);
	}
	else {
		element.className += ' '+className;
	}
}
function openMobileMenu() {
	document.body.style.overflowY = 'hidden';
	var headerNav = document.getElementById('nav-sito');
	addClass(headerNav, 'visible');
	setTimeout(function() {
		addClass(headerNav, 'open');
	}, 125);
}
function removeClass(element, className) {
	if (element.classList) {
		element.classList.remove(className);
	} else {
		element.className = element.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
	}
}
function closeMobileMenu() {
	document.body.style.overflowY = 'auto';
	var headerNav = document.getElementById('nav-sito');
	removeClass(headerNav, 'open');
	setTimeout(function() {
		removeClass(headerNav, 'visible');
	}, 334);
}
function insertHTMLForWidth() {
	if (window.innerWidth>=720) {
		if (document.getElementById('header-banner')==null) {
			document.getElementsByTagName('header')[0].insertAdjacentHTML('afterbegin', '<div id="header-banner" class="banner row"><figure class="first-figure no-margin"><img src="img/header-sx.jpeg" class="col-12 full-width-no-margin"></figure><a href="index.php" class="no-margin logo" title="Pagina principale"><img src="img/logo-lucasdelirium.png" alt="Lucasdelirium" class="full-width-no-margin"></a><figure class="no-margin"><img src="img/header-dx.jpeg" class="col-12 full-width-no-margin"></figure></div>');
			insertionsNumber++;
		}
	} else {
		if (document.getElementById('close-mobile-menu-icon')==null && document.getElementById('open-mobile-menu-icon')==null) {
			var headerNav = document.getElementById('nav-sito');
			document.getElementById('header-logo').insertAdjacentHTML('beforeend', '<span class="title">Lucasdelirium</span>');
			headerNav.insertAdjacentHTML('afterbegin', '<div class="col-12 full-width-no-margin close-icon-container"><span id="close-mobile-menu-icon" class="close-icon-wrapper"><img src="img/icona-chiudi.svg" alt="Chiudi"></span></div>');
			headerNav.insertAdjacentHTML('afterend', '<div id="open-mobile-menu-icon" class="mobile-menu-icon"><span class="1"></span><span class="2"></span><span class="3"></span></div>');
			document.getElementById('open-mobile-menu-icon').addEventListener('click', openMobileMenu);
			document.getElementById('close-mobile-menu-icon').addEventListener('click', closeMobileMenu);
			insertionsNumber++;
		}
	}
	if (insertionsNumber==2) {
		window.removeEventListener('resize', insertHTMLForWidth);
	}
}
insertionsNumber = 0;
window.addEventListener('resize', insertHTMLForWidth);
insertHTMLForWidth();
goUpButtonHref = document.getElementById('nav-articolo') ? 'nav-articolo' : 'contenuto';
goUpButtonTitle = goUpButtonHref=='nav-articolo' ? 'Clicca per tornare all\'inizio dell\'articolo.' : 'Clicca per tornare all\'inizio della pagina.';
document.body.insertAdjacentHTML('beforeend', '<a href="#'+goUpButtonHref+'" class="go-up-button" title="'+goUpButtonTitle+'"><img src="img/icona-torna-su.svg" alt="Torna su"></a>');
delete goUpButtonHref;
delete goUpButtonTitle;
function updateDocumentHeight() {
	documentHeight = Math.max(document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight);
}
var updateDocumentHeightTimeout = setTimeout(function() {updateDocumentHeight();}, 125);
window.addEventListener('resize', function() {
	clearTimeout(updateDocumentHeightTimeout);
	updateDocumentHeightTimeout = setTimeout(function() {updateDocumentHeight();}, 125);
});
function hasClass(element, className) {
	if (element.classList) {
		return element.classList.contains(className);
	} else {
		return new RegExp('(^| )' + className + '( |$)', 'gi').test(element.className);
	}
}
var hideGoUpButtonTimeoutStarted = false;
function hideGoUpButton() {
	var goUpButton = document.querySelectorAll('.go-up-button')[0];
	hideGoUpButtonTimeoutStarted = true;
	setTimeout(function() {removeClass(goUpButton, 'visible');}, 1834);
	setTimeout(function() {removeClass(goUpButton, 'render');}, 2134);
}
function checkScrolledToShowGoUpButton() {
	var goUpButton = document.querySelectorAll('.go-up-button')[0];
	if (document.documentElement.scrollTop>=documentHeight/3) {
		if (!hasClass(goUpButton, 'render')) {
			addClass(goUpButton, 'render');
			setTimeout(function() {addClass(goUpButton, 'visible');}, 34);
			hideGoUpButtonTimeoutStarted = false;
			if (window.innerWidth<720) {
				hideGoUpButton();
			}
		} else if (window.innerWidth<720 && !hideGoUpButtonTimeoutStarted) {
			hideGoUpButton();
		}
	} else if (hasClass(goUpButton, 'visible')) {
		removeClass(goUpButton, 'visible');
		setTimeout(function() {removeClass(goUpButton, 'render');}, 334);
	}
}
var checkScrolledToShowGoUpButtonTimeout = setTimeout(function() {checkScrolledToShowGoUpButton();}, 250);
window.addEventListener('scroll', function() {
	clearTimeout(checkScrolledToShowGoUpButtonTimeout);
	checkScrolledToShowGoUpButtonTimeout = setTimeout(function() {checkScrolledToShowGoUpButton();}, 250);
});
window.addEventListener('resize', checkScrolledToShowGoUpButton);
var lightboxElements = document.querySelectorAll('.lightbox a');
var lightboxElementsNumber = lightboxElements.length;
if (lightboxElementsNumber != 0) {
	document.body.insertAdjacentHTML('beforeend', '<div class="lightbox-background" aria-hidden="true"></div><figure class="lightbox-viewer no-margin" aria-hidden="true"><img class="loading" alt="Caricamento"><img class="loaded" alt=""><figcaption></figcaption></figure>');
	var lightboxBackground = document.querySelectorAll('.lightbox-background')[0];
	var lightboxViewer = document.querySelectorAll('.lightbox-viewer')[0];
	function closeLightbox(event) {
		var tagName = event.target.tagName.toLowerCase();
		if (tagName!='img' && tagName!='figcaption') {
			lightboxBackground.removeEventListener('click', closeLightbox);
			document.body.style.overflow = 'auto';
			removeClass(lightboxBackground, 'visible');
			removeClass(lightboxViewer, 'visible');
			setTimeout(function() {
				removeClass(lightboxBackground, 'render');
				removeClass(lightboxViewer, 'render');
				removeClass(lightboxViewer.children[0], 'render');
				removeClass(lightboxViewer.children[1], 'render');
			}, 334);
		}
	}
	function showLoadedAndHideLoading() {
			lightboxViewer.children[1].removeEventListener('load', showLoadedAndHideLoading);
			removeClass(lightboxViewer.children[0], 'render');
			addClass(lightboxViewer.children[1], 'render');
	}
	function openLightbox(event) {
		event.preventDefault();
		document.body.style.overflow = 'hidden';
		lightboxViewer.children[1].src = event.target.parentElement.href;
		lightboxViewer.children[0].src = event.target.parentElement.children[0].currentSrc;
		if (typeof(event.target.parentElement.parentElement.children[1])!=='undefined') {
			lightboxViewer.children[2].innerHTML = event.target.parentElement.parentElement.children[1].innerHTML;
		}
		addClass(lightboxViewer.children[0], 'render');
		addClass(lightboxBackground, 'render');
		setTimeout(function() {addClass(lightboxBackground, 'almost-visible');}, 34);
		addClass(lightboxViewer, 'render');
		setTimeout(function() {addClass(lightboxViewer, 'visible');}, 34);
		lightboxViewer.children[1].addEventListener('load', showLoadedAndHideLoading);
		lightboxBackground.addEventListener('click', closeLightbox);
		lightboxViewer.addEventListener('click', closeLightbox);
	}
	for (lightboxElementsIndex = 0; lightboxElementsIndex < lightboxElementsNumber; lightboxElementsIndex++) {
		lightboxElements[lightboxElementsIndex].addEventListener('click', openLightbox);
	}
	delete lightboxElementsIndex;
}
if (typeof articleBannerImg != "undefined"  && typeof previousArticle != "undefined" && typeof nextArticle != "undefined") {
	document.head.innerHTML += "\t\t<style>\n\t\t\t@media only screen and (min-width: 45em) {\n\t\t\t\t.row .article {\n\t\t\t\t\tmargin-left: 0;\n\t\t\t\t\tmargin-right: 0;\n\t\t\t\t}\n\t\t\t}\n\t\t</style>\n";
	function checkWindowWidthToAddArticleBannerAndNav() {
		if (window.innerWidth>=720 && document.getElementById('article-banner')==null) {
			window.removeEventListener('resize', checkWindowWidthToAddArticleBannerAndNav);
			document.getElementById('nav-articolo').insertAdjacentHTML('beforebegin', '<figure id="article-banner" class="col-12 hidden-sm banner"><img src="'+articleBannerImg+'" class="col-12 full-width-no-margin"></figure>');
			document.getElementsByClassName('article')[0].insertAdjacentHTML('beforebegin', '<a href="'+previousArticle+'" class="col-1 nav"><img src="img/icona-indietro.svg" alt="Indietro"></a>');
			document.getElementsByClassName('article')[0].insertAdjacentHTML('afterend', '<a href="'+nextArticle+'" class="col-1 nav"><img src="img/icona-avanti.svg" alt="Avanti"></a>');
			delete articleBannerImg;
			delete previousArticle;
			delete nextArticle;
		}
	}
	window.addEventListener('resize', checkWindowWidthToAddArticleBannerAndNav);
	checkWindowWidthToAddArticleBannerAndNav();
}
// @license-end
