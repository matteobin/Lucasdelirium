<?php ob_start(); ?>
<!doctype html>
<html lang="it">
	<head>
		<link rel="icon" href="img/logo-lucasdelirium.png">
		<link rel="stylesheet" href="stile.css">
	<?php if (isset($previousArticle) && $previousArticle): ?>
	<link rel="prev" href="<?php echo $previousArticle; ?>">
	<?php endif; ?>
<?php if (isset($nextArticle) && $nextArticle): ?>
	<link rel="next" href="<?php echo $nextArticle; ?>">
<?php endif; ?>
		<meta charset="utf-8">
		<meta name="author" content="Domenico Misciagna">
	<?php if (isset($description) && $description): ?>
	<meta name="description" content="<?php echo htmlspecialchars($description); ?>">
	<?php endif; ?>
	<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo $title; ?></title>
	</head>
	<body>
		<a href="#contenuto" class="skip-nav">Vai al contenuto</a>
		<header class="row">
			<div class="container row">
				<a id="header-logo" class="logo" href="index.php" title="Pagina principale"><img src="img/logo-lucasdelirium.png" alt="Lucasdelirium"></a>
				<nav id="nav-sito" aria-label="Indice sito">
					<ul>
						<li class="first"><a href="index.php">News</a></li>
						<li><a href="giochi.php">Schede dei giochi</a></li>
						<li><a href="lucaspedia.php">Lucaspedia</a></li>
						<li><a href="articoli.php">Articoli</a></li>
						<li><a href="file.php">File</a></li>
						<li><a href="missione.php">Missione</a></li>
						<li><a href="link.php">Link</a></li>
					</ul>
<?php include "socials-and-contacts.html"; ?>
				</nav>
			</div>
		</header>
		<div id="contenuto" class="container row">
